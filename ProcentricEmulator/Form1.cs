﻿using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;
using SuperSocket.WebSocket;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProcentricEmulator
{
    public partial class Form1 : Form
    {
        private WebSocketServer wsServer;
        private const int wsPort = 8053;

        public Form1()
        {
            InitializeComponent();
            Start();
        }

        private void Start()
        {
            wsServer = new WebSocketServer();
            if (!wsServer.Setup(wsPort))
            {
                MessageBox.Show(string.Format("Failed to listen on port {0}", wsPort));
                return;
            }

            wsServer.NewSessionConnected += new SessionHandler<WebSocketSession>(NewSessionConnected);

            wsServer.NewMessageReceived += new SessionHandler<WebSocketSession, string>(NewMessageReceived);

            if (!wsServer.Start())
            {
                MessageBox.Show(string.Format("Failed to start websocket server"));
                return;
            }
        }

        private void Stop()
        {
            if(wsServer != null)
            {
                wsServer.Stop();
            }
        }

        private void NewSessionConnected(WebSocketSession session)
        {
            labelClientCount.SetPropertyThreadSafe(() => labelClientCount.Text, wsServer.SessionCount.ToString());
        }

        static void NewMessageReceived(WebSocketSession session, string message)
        {

        }

        private void MinimizeToTray()
        {
            try
            {
                notifyIcon1.BalloonTipText = "Pro:Centric Emulator is running in background";
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(1000);
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RestoreFromTray()
        {
            this.Show();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                MinimizeToTray();
            }
        }

        private void toolStripMenuExit_Click(object sender, EventArgs e)
        {
            Stop();
            Application.Exit();
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            RestoreFromTray();
        }

        private void toolStripMenuRestore_Click(object sender, EventArgs e)
        {
            RestoreFromTray();
        }
    }
}
